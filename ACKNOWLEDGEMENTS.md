# Acknowledgements

This is a list of people who are not the project lead and who made tangible
contributions to the code base.

* Esperanto translation by Jade.
* Spanish translation contributed by sancocho.
