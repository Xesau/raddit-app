add_moderator:
    title: Add moderator to %forum%
    notice: The user has been promoted to moderator.

ban_add:
    title: Ban an IP address
    banned_notice: The IP address has been banned.

ban_form:
    ip: IP address
    reason: Reason for banning
    expiry_date: Expires at (YYYY-MM-DD hh:mm)
    ban: Ban
    user: User associated with IP

ban_list:
    title: Bans
    ip: IP address
    timestamp: Time
    banned_by: Banned by
    reason: Reason
    user: User
    user_none: none
    expires: Expires
    expires_never: never
    remove: Remove bans

banned:
    title: Banned
    message: You are banned!

comments:
    author_deleted: '[deleted]'
    comments: Comments
    delete: Delete
    delete_thread: Delete with replies
    edit: Edit
    permalink: Permalink
    parent: Parent
    reply: Reply
    admin_ban: IP ban
    form_load_error: Error loading form. Please try again.
    info: '%user% wrote %timestamp%'
    info_at_timestamp: at %timestamp%
    not_logged_in: You must %login_link% or %register_link% to comment.
    not_logged_in_login_link_label: log in
    not_logged_in_register_link_label: register
    viewing_thread: Viewing a single comment thread
    thread_return: View all comments
    return_to_forum: ← Return to %forum%

comment_form:
    comment: Comment
    submit: Submit
    edit_title: Editing comment
    create_title: Posting a comment

create_forum:
    create_new_forum: Create new forum

edit_forum:
    edit_notice: The changes have been saved.
    delete_notice: The forum and all its contents have been deleted.
    title: Editing %forum%

edit_user:
    title: Editing user %username%

forum:
    moderators: Moderators
    manage: Manage
    edit: Edit forum
    subscribe: Subscribe
    unsubscribe: Unsubscribe
    subscriber_count: '{0} No subscribers|{1} %count% subscriber|[1,Inf[ %count% subscribers'
    add_moderator: Add moderator

forum_form:
    name: Name
    title: Title
    description: Description
    create: Create forum
    save: Save changes
    delete: Delete forum
    confirm_delete: Are you sure you want to delete this forum and all its submissions and comments?
    featured: Show on front page

forum_list:
    name: Name
    title: Title
    subscribers: Subscribers
    page_title: List of forums
    create_forum: Create forum

forum_moderators:
    title: Moderators for /%forum_name%
    username: Username
    since: Since

front:
    subscribed_forums: Subscribed forums
    no_subscriptions: You are not subscribed to any forum. Showing featured forums instead.
    featured_forums: Featured forums
    no_forums: There are no featured forums to display.
    front: Front
    all: All

inbox:
    title: Inbox
    clear_inbox: Clear inbox
    clear_notice: The inbox was cleared.
    empty: The inbox is empty.

login_form:
    log_in: Log in
    username: Username
    password: Password
    reset_password: Reset password?

markdown_type:
    help: Syntax reference

moderator_form:
    user: Username
    submit: Add as moderator

pagination:
    next: Next
    previous: Previous

request_password_reset:
    title: Request password reset link
    multiple_accounts_notice: >
        If you have several accounts registered to one email address, you will
        receive one email per account.
    no_confirmation_notice: >
        If you never receive an email, the address you specified is probably
        incorrect. For privacy reasons we will not confirm if a user with the
        specified email address actually exists.

request_password_reset_form:
    email: Email address
    submit: Submit

reset_password:
    email_subject: '%site_name% - Reset password for user %username%'

    # this should be kept at 76 columns --------------------------------------------
    email_body: |
        Someone requested a password reset for your %site_name% account. To reset
        your password, click the link below:

        %reset_link%

        If you did not request a password reset, you can safely ignore this message.
        The link will expire after 24 hours.

    email_notice: A reset email has been to the address you specified.
    update_notice: Your password has been updated.
    title: Resetting password

site_footer:
    version: Running %app% %version% (%branch%). Made with ☭.

site_nav:
    submit: Submit
    log_in: Log in
    log_out: Log out
    profile: Profile
    register: Register
    my_account: My account
    user_settings: User settings
    forum_list: Forums
    inbox: Inbox (%count%)

submission_form:
    title: Title
    url: URL
    body: Body
    forum: Forum
    sticky: Sticky
    create: Create submission
    edit: Edit submission
    delete: Delete submission
    confirm_delete: Are you sure you want to delete this submission?

submissions:
    comments: '{0} No comments|{1} %count% comment|[1,Inf[ %count% comments'
    delete_notice: The submission was deleted.
    edit: Edit
    edit_notice: The submission was edited.
    info_with_forum_name: Submitted by %submitter% %timestamp% in %forum%
    info_without_forum_name: Submitted by %submitter% %timestamp%
    info_at_timestamp: at %timestamp%
    sort_by_hot: Hot
    sort_by_new: New
    sort_by_top: Top
    sort_by_controversial: Controversial
    total_votes: '{1} %count% vote|[0,Inf[ %count% votes'
    vote_stats: (+%up%, −%down%)
    ip_ban: IP ban

votes:
    upvote: Upvote
    downvote: Downvote
    retract_upvote: Retract upvote
    retract_downvote: Retract downvote

user:
    submissions: Submissions
    comments: Comments
    moderates: '%username% is a moderator on:'

user_form:
    username: Username
    password: Password
    repeat_password: Password (repeat)
    new_password: New password
    repeat_new_password: New password (repeat)
    email: Email
    register: Register
    save: Save changes

user_settings:
    title: Editing user settings for %username%
    update_notice: User settings have been updated.

user_settings_form:
    locale: Language
    night_mode: Night mode
    save: Save changes
